[comment]: # (This presentation was made with markdown-slides)
[comment]: # (This is a CommonMark compliant comment. It will not be included in the presentation.)
[comment]: # (Compile this presentation with the command below)
[comment]: # (mdslides presentation.md --include media)

[comment]: # (Set the theme:)
[comment]: # (THEME = black)
[comment]: # (CODE_THEME = base16/zenburn)
[comment]: # (The list of themes is at https://revealjs.com/themes/)
[comment]: # (The list of code themes is at https://highlightjs.org/)

[comment]: # "You can also use quotes instead of parenthesis"
[comment]: # "THEME = black"

[comment]: # (Pass optional settings to reveal.js:)
[comment]: # (controls: true)
[comment]: # (keyboard: true)
[comment]: # (markdown: { smartypants: true })
[comment]: # (hash: false)
[comment]: # (respondToHashChanges: false)
[comment]: # (Other settings are documented at https://revealjs.com/config/)

Jonathan Gerhard Tarigan | Kepala Balai Sertifikasi Elektronik | Senin, 22 Agustus 2022

### "Work From Anywhere" dengan Aplikasi SIKD

[comment]: # (A comment starting with three or more !!! marks a slide break.)
[comment]: # (!!! data-background-video="media/neon.mp4", data-background-video-loop data-background-video-muted data-background-opacity="1")

#### Regulasi terkait [Sertifikat Elektronik](https://www.google.com/search?q=sertifikat+elektronik) dan [Tanda Tangan Elektonik](https://www.google.com/search?q=tanda+tangan+elektronik)

[comment]: # (|||)

[**Inpres 6/2001 tentang Pengembangan dan Pendayagunaan Telematika di Indonesia**](https://www.google.com/search?q=inpres+6+2001)

Arahan perumusan produk-produk hukum baru di bidang telematika *cyber law* yang mengatur keabsahan dokumen elektronik tandatangan digital, pembayaran secara elektronik, otoritas sertifikasi, kerahasiaan, dan keamanan pemakai layanan jaringan informasi (poin 13)

[comment]: # (|||)

[**Inpres 3/2003 tentang Kebijakan dan Strategi Nasional Pengembangan e-Government**](https://www.google.com/search?q=inpres+3+2003)

Arahan perumusan kebijakan tentang pengamanan informasi serta pembakuan sistem otentikasi dan *public key infrastructure* untuk menjamin keamanan informasi dalam penyelenggaraan transaksi dengan pihak-pihak lain, terutama yang berkaitan dengan kerahasiaan informasi dan transaksi finansial (Strategi 3.c)

[comment]: # (|||)

[**UU 11/2008 tentang Informasi dan Transaksi Elektronik**](https://www.google.com/search?q=uu+11+2008)

Kebijakan tanda tangan elektronik dan sertifikat elektronik (Pasal 5 s.d pasal 14)

[comment]: # (|||)

[**Perpres 95/2018 tentang Sistem Pemerintahan Berbasis Elektronik**](https://www.google.com/search?q=perpres+95+2018)

Penerapan tanda tangan digital dan jaminan pihak ketiga terpercaya melalui penggunaan sertifikat digital (Pasal 40)

[comment]: # (!!!)

#### Amanat [**Perpres 95/2018**](https://www.google.com/search?q=perpres+95+2018)
#### Sistem Pemerintah Berbasis Elektronik

[comment]: # (|||)

[**Pasal 40 ayat 1**]()

Keamanan SPBE mencakup penjaminan kerahasiaan, [**keutuhan**](), ketersediaan, [**keaslian**](), [**kenirsangkalan**]() (*non repudiation*) sumber daya terkait data dan informasi, infrastruktur SPBE, dan aplikasi SPBE.

[comment]: # (|||)

[**Pasal 40 ayat 3**]()

Penjaminan keutuhan sebagaimana dimaksud pada ayat (1) dilakukan melalui [**pendeteksian modifikasi**]().

[comment]: # (|||)

[**Pasal 40 ayat 5**]()

Penjaminan keaslian sebagaimana dimaksud pada ayat (1) dilakukan melalui penyediaan mekanisme [**verifikasi dan validasi**]().

[comment]: # (|||)

[**Pasal 40 ayat 6**]()

Penjaminan [**kenirsangkalan**]() (*non repudiation*) sebagaimana dimaksud pada ayat (1) dilakukan melalui penerapan [**tanda tangan digital**]() dan [**jaminan pihak ketiga terpercaya**]() melalui penggunaan [**sertifikat digital**]().

[comment]: # (!!!)

#### sertifikat elektronik sebagai [**identitas digital**]()

[comment]: # (|||)

![picture of id card](media/id%20card.png) <!-- .element: style="height:50vh; max-width:80vw; image-rendering: crisp-edges;" -->

Paspor, KTP, SIM, dll

[comment]: # (|||)

![picture of sre](media/sre.png) <!-- .element: style="height:50vh; max-width:80vw; image-rendering: crisp-edges;" -->

Sertifikat Elektronik

[comment]: # (!!!)

## Terima Kasih

----------

<div style="font-size: 0.5em;">
- Balai Sertifikasi Elektronik -
</div>